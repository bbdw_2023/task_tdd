# História de Usuário 1: Adicionar Tarefa
Como um usuário,
Eu quero adicionar uma nova tarefa na minha lista de tarefas,
Para que eu possa manter um registro das coisas que preciso fazer.

### Critérios de Aceitação:

    O usuário deve ser capaz de inserir o título e a descrição da tarefa.
    O usuário deve ser capaz de definir a prioridade da tarefa.
    A tarefa adicionada deve aparecer na lista de tarefas pendentes.

# História de Usuário 2: Atualizar Tarefa
Como um usuário,
Eu quero atualizar os detalhes de uma tarefa existente,
Para que eu possa corrigir erros ou alterar os prazos.

### Critérios de Aceitação:

    O usuário deve ser capaz de modificar o título, descrição e prioridade de uma tarefa.
    O usuário deve ser capaz de salvar as alterações feitas.
    As alterações devem ser refletidas imediatamente na interface do usuário.

# História de Usuário 3: Excluir Tarefa
Como um usuário,
Eu quero excluir uma tarefa da minha lista,
Para que eu possa manter minha lista organizada e livre de tarefas que não são mais necessárias.

### Critérios de Aceitação:

    O usuário deve ser capaz de remover uma tarefa com um gesto de deslizar.
    Deve haver uma confirmação antes de excluir permanentemente a tarefa.
    A tarefa excluída não deve mais aparecer na lista.

# História de Usuário 4: Marcar Tarefa Como Concluída
Como um usuário,
Eu quero marcar uma tarefa como concluída,
Para que eu possa visualizar o meu progresso e focar nas tarefas que ainda estão pendentes.

### Critérios de Aceitação:

    O usuário deve ser capaz de marcar uma tarefa como concluída com um único toque.
    A tarefa concluída deve ser movida para a seção ou lista de tarefas concluídas.
    A tarefa concluída deve ser visualmente distinta das tarefas pendentes.

# História de Usuário 5: Listar Tarefas
Como um usuário,
Eu quero ver uma lista de todas as minhas tarefas pendentes e concluídas,
Para que eu possa facilmente organizar o meu dia.

### Critérios de Aceitação:

    O usuário deve ser capaz de ver as tarefas organizadas por status: pendentes e concluídas.
    O usuário deve ser capaz de ver as informações essenciais da tarefa na lista.
    A lista deve ser atualizada em tempo real após qualquer operação.