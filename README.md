# task_tdd

Aplicativo de Lista de Tarefas (Todo App)

## Getting Started

O aplicativo permitirá que os usuários criem, atualizem, excluam e listem tarefas. Cada tarefa pode ter prioridades e etiquetas.

### Componentes Principais:

    Modelo de Tarefa (Task Model):
        Representa uma tarefa com propriedades como id, title, description, priority, labels, e isComplete.

    Gerenciador de Tarefas (Task Manager):
        Lógica de negócios para adicionar, remover e atualizar tarefas.
        Utiliza o padrão de repositório para desacoplar a lógica de armazenamento.

    Repositório de Tarefas (Task Repository):
        Interface para definir operações de dados.
        Implementações concretas para armazenamento em memória, banco de dados ou rede.

    Widgets de UI:
        TaskListWidget: Lista as tarefas com opções para ordenar e filtrar.
        TaskItemWidget: Exibe informações individuais de tarefa com opções para marcar como completa ou editar.
        TaskEditWidget: Formulário para adicionar ou editar uma tarefa.

### Processo TDD:

    Testes de Modelo: Comece criando testes para o modelo de Tarefa, garantindo que ele possa ser serializado e desserializado corretamente.

    Testes de Gerenciador de Tarefas: Escreva testes para a lógica de adicionar, remover e atualizar tarefas, bem como para as regras de negócios (como validação de prioridades e etiquetas).

    Testes de Repositório: Defina os testes para as operações de repositório, como salvar e recuperar tarefas, sem se preocupar com a implementação concreta.

    Testes de Widgets: Crie testes de widget para cada componente da UI, garantindo que eles respondam adequadamente às interações do usuário e mudanças de estado.

    Desenvolvimento Guiado por Testes: Para cada conjunto de testes, escreva o código mínimo necessário para passar nos testes.

    Refatoração: Com os testes passando, refatore o código para atender aos princípios SOLID e garantir um design de arquitetura limpo.

### Resultados Esperados com TDD:

    Documentação Viva: Os testes atuam como uma documentação que demonstra o uso e as expectativas da aplicação.
    Design de Arquitetura Limpo: TDD força a criação de interfaces claras e desacoplamento, facilitando a manutenção e escalabilidade.
    Princípios SOLID: O processo encoraja a aderência aos princípios SOLID para um código mais robusto e flexível.
