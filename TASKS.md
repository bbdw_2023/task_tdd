# Passos

## Feito:

## Fazendo:
 
## A fazer:
1. Testes de Modelo: Comece criando testes para o modelo de Tarefa, garantindo que ele possa ser serializado e desserializado corretamente.
2. Testes de Gerenciador de Tarefas: Escreva testes para a lógica de adicionar, remover e atualizar tarefas, bem como para as regras de negócios (como validação de prioridades e etiquetas).
3. Testes de Repositório: Defina os testes para as operações de repositório, como salvar e recuperar tarefas, sem se preocupar com a implementação concreta.
4. Testes de Widgets: Crie testes de widget para cada componente da UI, garantindo que eles respondam adequadamente às interações do usuário e mudanças de estado.

# SEMPRE
- Refatoração: Com os testes passando, refatore o código para atender aos princípios SOLID e garantir um design de arquitetura limpo.

